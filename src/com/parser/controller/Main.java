package com.parser.controller;

import java.io.IOException;

import com.parser.view.Gui;

public class Main {

    public static void main(String[] args) throws IOException {
        Controller controller = new Controller();
        Gui gui = new Gui(controller);
        gui.show();
    }

}
