package com.parser.controller;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class Controller {

    private final List<List<String>> listCache;

    public Controller() {
        this.listCache = new ArrayList<>();
    }

    public void setFromFile() throws IOException {
        Reader in = new FileReader(Main.class.getResource("/files/toPars.csv").getFile());
        for (CSVRecord record : CSVFormat.DEFAULT.withDelimiter('|').withDelimiter(':').parse(in)) {
            List<String> lista = new ArrayList<>();
            for (String field : record) {
                // System.out.print("\"" + field + "\", ");
                lista.add(field);
            }
            this.listCache.add(lista);
            // System.out.println();
        }
        System.out.println(listCache);
    }

    public List<List<String>> getCache() {
        return this.listCache;
    }
}
