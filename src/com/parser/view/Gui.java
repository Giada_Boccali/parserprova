package com.parser.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.parser.controller.Controller;

public class Gui extends ViewSkeleton implements ActionListener {

    Controller controller;

    public Gui(final Controller controller) {
        this.controller = controller;
        this.getBott1().addActionListener(this);
        this.getBott2().addActionListener(this);
        this.getBott3().addActionListener(this);
        this.getBott4().addActionListener(this);
        this.getSetButton().addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source.equals(this.getBott1())) {
            try {
                this.chargeEverything(0);
            } catch (IndexOutOfBoundsException e2) {
                this.showCacheError();
            }
        } else if (source.equals(this.getBott2())) {
            try {
                this.chargeEverything(1);
            } catch (IndexOutOfBoundsException e2) {
                this.showCacheError();
            }
        } else if (source.equals(this.getBott3())) {
            try {
                this.chargeEverything(2);
            } catch (IndexOutOfBoundsException e2) {
                this.showCacheError();
            }
        } else if (source.equals(this.getBott4())) {
            try {
                this.chargeEverything(3);
            } catch (IndexOutOfBoundsException e2) {
                this.showCacheError();
            }
        } else if (source.equals(this.getSetButton())) {
            try {
                this.controller.setFromFile();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void chargeEverything(final int n) throws IndexOutOfBoundsException {
        switch (n) {
        case 0:
            this.getTesto1().setText(this.controller.getCache().get(0).get(0));
            this.getBott1().setLabel(this.controller.getCache().get(0).get(1));
            break;
        case 1:
            this.getTesto2().setText(this.controller.getCache().get(1).get(0));
            this.getBott2().setLabel(this.controller.getCache().get(1).get(1));
            break;
        case 2:
            this.getTesto3().setText(this.controller.getCache().get(2).get(0));
            this.getBott3().setLabel(this.controller.getCache().get(2).get(1));
            break;
        case 3:
            this.getTesto4().setText(this.controller.getCache().get(3).get(0));
            this.getBott4().setLabel(this.controller.getCache().get(3).get(1));
            break;
        }
    }

    public void show() {
        this.getFrame().setVisible(true);
    }

    public void showCacheError() {
        JOptionPane.showMessageDialog(this.getFrame(), "Non hai inializzato la cache dal file");
    }
}
