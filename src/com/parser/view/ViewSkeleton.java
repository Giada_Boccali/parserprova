package com.parser.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextField;

import javax.swing.JFrame;

public class ViewSkeleton {
    private myFrame frame;
    private Panel pannello;
    private Panel pannello2;
    private Panel mainPane;

    private TextField testo1;
    private TextField testo2;
    private TextField testo3;
    private TextField testo4;

    private Button bott1;
    private Button bott2;
    private Button bott3;
    private Button bott4;

    private Button setButton;

    public ViewSkeleton() {
        this.frame = new myFrame();
        this.mainPane = new Panel(new BorderLayout());
        this.pannello = new Panel(new GridLayout(4, 0));
        this.pannello2 = new Panel(new GridLayout(4, 0));
        this.mainPane.add(pannello, BorderLayout.CENTER);
        this.mainPane.add(pannello2, BorderLayout.EAST);

        this.testo1 = new TextField();
        this.testo2 = new TextField();
        this.testo3 = new TextField();
        this.testo4 = new TextField();

        this.bott1 = new Button("Bottone1");
        this.bott2 = new Button("Bottone2");
        this.bott3 = new Button("Bottone3");
        this.bott4 = new Button("Bottone4");

        this.setButton = new Button("Set Cache da file");
        this.mainPane.add(setButton, BorderLayout.NORTH);

        this.pannello.add(testo1);
        this.pannello.add(testo2);
        this.pannello.add(testo3);
        this.pannello.add(testo4);

        this.pannello2.add(bott1);
        this.pannello2.add(bott2);
        this.pannello2.add(bott3);
        this.pannello2.add(bott4);

        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.getContentPane().add(mainPane);
    }

    /**
     * @return the testo1
     */
    public TextField getTesto1() {
        return testo1;
    }

    /**
     * @return the testo2
     */
    public TextField getTesto2() {
        return testo2;
    }

    /**
     * @return the testo3
     */
    public TextField getTesto3() {
        return testo3;
    }

    /**
     * @return the testo4
     */
    public TextField getTesto4() {
        return testo4;
    }

    /**
     * @return the frame
     */
    public myFrame getFrame() {
        return frame;
    }

    /**
     * @return the bott1
     */
    public Button getBott1() {
        return bott1;
    }

    /**
     * @return the bott2
     */
    public Button getBott2() {
        return bott2;
    }

    /**
     * @return the bott3
     */
    public Button getBott3() {
        return bott3;
    }

    /**
     * @return the bott4
     */
    public Button getBott4() {
        return bott4;
    }

    /**
     * @return the setButton
     */
    public Button getSetButton() {
        return setButton;
    }

}
