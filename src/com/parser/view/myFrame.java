package com.parser.view;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class myFrame extends JFrame {

    /**
     * The default serial id
     */
    private static final long serialVersionUID = 1L;
    Dimension dimensioni;

    public myFrame() {
        this.setTitle("Parser");
        this.dimensioni = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(dimensioni.width / 3, dimensioni.height / 3);
    }
}
